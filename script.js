'use strict'

let score = 0
let mistakes = 0

// [is_white?, name_#, name_b]
let notes = []

// separate array for the names of the notes to allow to choose between german and english names
let note_names_german = [['c', 'cis', 'd', 'dis', 'e', 'f', 'fis', 'g', 'gis', 'a', 'ais', 'h'],
  ['c', 'des', 'd', 'es', 'e', 'f', 'ges', 'g', 'aes', 'a', 'b', 'h']]
let note_names_english = [['c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b'],
  ['c', 'db', 'd', 'eb', 'e', 'f', 'gb', 'g', 'ab', 'a', 'bb', 'b']]

// color of the notes given in note_names_*, true for white
let note_colors = [true, false, true, false, true, true, false, true, false, true, false, true]

// index of the solution
let solution = null

/**
 * initialise the site and all functionality
 *
 * @param {String} language - 'de' for german, anything else for english
 */
function init (language='en') {
  console.log('init start')

  // put the names in the notes array
  for (let i = 0; i < notes.length; i++) {
    notes[i][0] = note_colors[i]
    if (language === 'de') {
      notes[i][1] = note_names_german[0][i]
      notes[i][2] = note_names_german[1][i]
    } else {
      notes[i][1] = note_names_english[0][i]
      notes[i][2] = note_names_english[1][i]
    }
  }

  // display note buttons
  displayNoteButtons()

  console.log('init end')
}

/**
 * generate the list of buttons for the notes
 *
 * @param {Number} offset - the offset of the first note relative to c
 * @param {Boolean} sharp - use display notes as #? (f# vs. gb)
 */
function displayNoteButtons(offset=0, sharp=true) {
  if (offset >= notes.length || offset < 0)
    throw RangeError(`the given offset has to be between 0 and ${notes.length}. given: ${offset}`)

  let index = offset
  let fragment = document.createDocumentFragment()
  for (let i = 0; i < notes.length; i++) {
    // generate the container for the button
    let container = document.createElement('div')
    container.classList.add('col-1')

    // generate the button
    let button = document.createElement('button')
    button.type = 'button'
    button.id = 'note' + index
    button.classList.add('btn')
    button.classList.add('btn-block')
    if (notes[index][0])
      // white key
      button.classList.add('btn-light')
    else
      // black key
      button.classList.add('btn-dark')
    if (sharp)
      button.textContent = notes[index][1]
    else
      button.textContent = notes[index][2]

    // append the button to the container item
    container.appendChild(button)

    // append the container to the fragment
    fragment.appendChild(container)

    // increment the index and correct overflows
    index++
    if (index > notes.length)
      index -= notes.length
  }

  // remove the old content of the root and append the fragment to the root object for the notes
  let root = document.getElementById('notes_root')
  root.innerHTML = ''
  root.appendChild(fragment)
}

function newChallenge () {
  // TODO: replace dummy
  solution = 0
}

function testChallenge (index) {

}

init('de')
